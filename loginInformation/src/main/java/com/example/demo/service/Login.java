package com.example.demo.service;

import java.util.Random;

import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
//import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Login {

	int count = 0;

	@Retryable(value = { ArithmeticException.class }, maxAttempts = 3)
	@GetMapping(value = "/numbers")
	public ResponseEntity<Integer> validateNumber() throws ArithmeticException {
		Random rand = new Random();
		System.out.println("Trying for " + ++count + " time!");
		int rand_int1 = rand.nextInt(1000);
		if (true) {
			throw new ArithmeticException();
		}
		return ResponseEntity.status(200).body(rand_int1);

	}

	@Recover
	public ResponseEntity<String> recoverMethod() {
		return ResponseEntity.status(200).body("Try after some time");

	}

}
